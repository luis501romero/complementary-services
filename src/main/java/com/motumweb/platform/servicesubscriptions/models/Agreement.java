package com.motumweb.platform.servicesubscriptions.models;

import java.util.Date;

public class Agreement {
	
	private Integer agreementId;
	private Integer clientServiceId;
	private Integer agreementTypeId;
	private Integer billingPlanId;
	private Integer agreementStatusId;
	private Date startDate;
	private Date endDate;
	private Date cancellationDate;
	private Date procesingDate;
	
	public Integer getAgreementId() {
		return agreementId;
	}
	public void setAgreementId(Integer agreementId) {
		this.agreementId = agreementId;
	}
	public Integer getClientServiceId() {
		return clientServiceId;
	}
	public void setClientServiceId(Integer clientServiceId) {
		this.clientServiceId = clientServiceId;
	}
	public Integer getAgreementTypeId() {
		return agreementTypeId;
	}
	public void setAgreementTypeId(Integer agreementTypeId) {
		this.agreementTypeId = agreementTypeId;
	}
	public Integer getBillingPlanId() {
		return billingPlanId;
	}
	public void setBillingPlanId(Integer billingPlanId) {
		this.billingPlanId = billingPlanId;
	}
	public Integer getAgreementStatusId() {
		return agreementStatusId;
	}
	public void setAgreementStatusId(Integer agreementStatusId) {
		this.agreementStatusId = agreementStatusId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getCancellationDate() {
		return cancellationDate;
	}
	public void setCancellationDate(Date cancellationDate) {
		this.cancellationDate = cancellationDate;
	}
	public Date getProcesingDate() {
		return procesingDate;
	}
	public void setProcesingDate(Date procesingDate) {
		this.procesingDate = procesingDate;
	}
}
