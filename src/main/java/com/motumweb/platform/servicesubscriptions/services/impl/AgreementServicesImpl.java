package com.motumweb.platform.servicesubscriptions.services.impl;

import org.springframework.stereotype.Service;

import com.motumweb.platform.servicesubscriptions.daos.AgreementDAO;
import com.motumweb.platform.servicesubscriptions.daos.ServiceDAO;
import com.motumweb.platform.servicesubscriptions.dtos.AgreementDTO;
import com.motumweb.platform.servicesubscriptions.exceptions.NotFoundException;
import com.motumweb.platform.servicesubscriptions.models.Agreement;
import com.motumweb.platform.servicesubscriptions.services.AgreementServices;

@Service
public class AgreementServicesImpl implements AgreementServices {
	
	private AgreementDAO agreementDAO;
	
	public AgreementServicesImpl(AgreementDAO agreementDAO) {
		this.agreementDAO = agreementDAO;
	}

	@Override
	public void saveAgreement(Integer clientId, Integer userId, AgreementDTO agreement) throws NotFoundException {
		Agreement newAgreement = new Agreement();
		Integer clientServiceId = this.agreementDAO.findClientServiceByType(agreement.getClientService());
		Integer agreementTypeId = this.agreementDAO.findAgreementByType(agreement.getAgreementType());
		Integer billingPlanId = this.agreementDAO.findBillingPlanByType(agreement.getBillingPlan());
		Integer agreementStatusId = this.agreementDAO.findAgreementStatusByStatus(agreement.getAgreementStatus());
		newAgreement.setClientServiceId(clientServiceId);
		newAgreement.setAgreementTypeId(agreementTypeId);
		newAgreement.setBillingPlanId(billingPlanId);
		newAgreement.setAgreementStatusId(agreementStatusId);
		newAgreement.setStartDate(agreement.getStartDate());
		newAgreement.setEndDate(agreement.getEndDate());
		newAgreement.setCancellationDate(agreement.getCancellationDate());
		newAgreement.setProcesingDate(agreement.getProcesingDate());
		String error = "Missing data or is incorrect";
		String code = "INCORRECT_DATA";
		System.out.println(agreement.getAgreementType());

		try {
			if(!this.agreementDAO.checkTermsConditions(userId, clientId)) {
				code = "NOT_ACCEPTED_TERMS_CONDITIONS";
				error = "The terms and conditions is not accepted";
				throw new NotFoundException(code, error);
			}
			if(this.agreementDAO.existAgreement(userId, clientId, clientServiceId)) {
				System.out.println("Existe");
				Agreement agreementExist = new Agreement();
				agreementExist = this.agreementDAO.findAgreementByService(clientId, userId, clientServiceId);
				if(this.agreementDAO.findAgreementType(agreementExist.getAgreementTypeId()).equals("Demo") && agreement.getAgreementType().equals("Indeterminado")) {
					this.agreementDAO.updateDemoAgreement(this.agreementDAO.findAgreementId(clientId, userId, clientServiceId));
					System.out.println("Demo");
					return;
				}
				if(agreement.getAgreementType().equals("Demo")) {
					code = "AGREEMENT_AL_READY_EXIST";
					error = "Already exist an agreement for this user";
					System.out.println(error);
					throw new NotFoundException(code, error);

				}
				if(this.agreementDAO.findAgreementType(agreementExist.getAgreementTypeId()).equals("Indeterminado") && agreement.getAgreementType().equals("Indeterminado")) {
					code = "AGREEMENT_AL_READY_EXIST";
					error = "Already exist an agreement for this user";
					System.out.println(error);

					throw new NotFoundException(code, error);
				}
			}
			this.agreementDAO.saveAgreement(clientId, userId, newAgreement);
		} catch (Exception e) {
			throw new NotFoundException(code, error);
		}
	}

}
