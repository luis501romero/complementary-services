package com.motumweb.platform.servicesubscriptions.services.impl;

import org.springframework.stereotype.Service;

import com.motumweb.platform.servicesubscriptions.daos.ServiceDAO;
import com.motumweb.platform.servicesubscriptions.dtos.ServiceDTO;
import com.motumweb.platform.servicesubscriptions.exceptions.NotFoundException;
import com.motumweb.platform.servicesubscriptions.models.Agreement;
import com.motumweb.platform.servicesubscriptions.services.ServiceServices;

@Service
public class ServiceServicesImpl implements ServiceServices{
	
	private ServiceDAO serviceDAO;
	
	public  ServiceServicesImpl(ServiceDAO serviceDAO) {
		this.serviceDAO = serviceDAO;
	}

	@Override
	public ServiceDTO getAgreementByServiceIdentifier(Integer clientId, Integer userId, Integer serviceId) throws NotFoundException {
		ServiceDTO agreementDTO = new ServiceDTO();
		Agreement agreement = this.serviceDAO.getAgreementByServiceIdentifier(clientId, userId, serviceId);
		if(agreement != null) {
			agreementDTO.setServiceId(agreement.getClientServiceId());
			agreementDTO.setService(this.serviceDAO.findClientServiceName(serviceId));
			agreementDTO.setAgreementTypeId(agreement.getAgreementTypeId());
			agreementDTO.setAgreementType(this.serviceDAO.findAgreementType(agreement.getAgreementTypeId()));
			agreementDTO.setStartDate(agreement.getStartDate());
			agreementDTO.setEndDate(agreement.getEndDate());
			return agreementDTO;
		} else {
			throw new NotFoundException("SERVICE_NOT_FOUND","Service not found");
		}
	}
}
