package com.motumweb.platform.servicesubscriptions.daos.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.motumweb.platform.servicesubscriptions.daos.ServiceDAO;
import com.motumweb.platform.servicesubscriptions.models.Agreement;

@Repository
public class ServiceDAOImpl implements ServiceDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedJdbc;
	
	public Agreement getAgreementByServiceIdentifier(Integer clientId, Integer userId, Integer serviceId) {
		String sql = "SELECT agreement_id, client_service_id, agreement_type_id, billing_plan_id, \n"
				+ "client_id, agreement_status_id, user_id, start_date, end_date, cancellation_date, procesing_date\n"
				+ "FROM client_services.agreement WHERE client_id = :clientId  AND user_id = :userId  AND client_service_id = :serviceId ";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("clientId", clientId)
				.addValue("userId", userId)
				.addValue("serviceId", serviceId);
		try {
			return (Agreement) this.namedJdbc.queryForObject(sql, params, new BeanPropertyRowMapper(Agreement.class));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public String findClientServiceName(Integer serviceId) {
		String sql = "SELECT \"type\" \n"
				+ "FROM client_services.client_service WHERE client_service_id = :serviceId";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("serviceId", serviceId);
		try {
			return this.namedJdbc.queryForObject(sql, params, String.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public String findAgreementType(Integer agreementId) {
		String sql = "SELECT \"type\" \n"
				+ "FROM client_services.agreement_type WHERE agreement_type_id = :agreementId";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("agreementId", agreementId);
		try {
			return this.namedJdbc.queryForObject(sql, params, String.class);
		} catch (Exception e) {
			return null;
		}
	}
}
