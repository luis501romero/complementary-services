package com.motumweb.platform.servicesubscriptions.daos;

import com.motumweb.platform.servicesubscriptions.models.Agreement;

public interface ServiceDAO {
	
	public Agreement getAgreementByServiceIdentifier(Integer clientId, Integer userId, Integer serviceId);
	
	public String findClientServiceName(Integer serviceId);
	
	public String findAgreementType(Integer agreementId);

}
