package com.motumweb.platform.servicesubscriptions.daos;

import com.motumweb.platform.servicesubscriptions.models.Agreement;

public interface AgreementDAO {

	public void saveAgreement(Integer clientId, Integer userId, Agreement agreement);
	
	public Integer findClientServiceByType(String serviceType);
	
	public Integer findAgreementByType(String agreementType);

	public Integer findBillingPlanByType(String billingPlanType);
	
	public Integer findAgreementStatusByStatus(String agreementStatus);
	
	public Boolean checkTermsConditions(Integer userId, Integer clientId);
	
	public Boolean existAgreement(Integer userId, Integer clientId, Integer serviceId);
	
	public void updateDemoAgreement(Integer agreementId);
	
	public Agreement findAgreementByService(Integer clientId, Integer userId, Integer serviceId);
	
	public String findAgreementType(Integer agreementTypeId);
	
	public Integer findAgreementId(Integer clientId, Integer userId, Integer serviceId);
}
