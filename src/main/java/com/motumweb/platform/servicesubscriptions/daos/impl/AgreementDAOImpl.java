package com.motumweb.platform.servicesubscriptions.daos.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.motumweb.platform.servicesubscriptions.daos.AgreementDAO;
import com.motumweb.platform.servicesubscriptions.models.Agreement;

@Repository
public class AgreementDAOImpl implements AgreementDAO {
	
	@Autowired
	private NamedParameterJdbcTemplate namedJdbc;

	@Override
	public void saveAgreement(Integer clientId, Integer userId, Agreement agreement) {
		String sql = "INSERT INTO client_services.agreement(client_service_id, agreement_type_id, billing_plan_id, \n"
				+ "client_id, agreement_status_id, user_id, start_date, end_date, cancellation_date, procesing_date)\n"
				+ "VALUES (:clientServiceId, :agreementTypeId, :billingPlanId, :clientId, :agreementStatusId, :userId, :startDate, :endDate, :cancellationDate, :procesingDate)";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("clientServiceId", agreement.getClientServiceId())
				.addValue("agreementTypeId", agreement.getAgreementTypeId())
				.addValue("billingPlanId", agreement.getBillingPlanId())
				.addValue("clientId", clientId)
				.addValue("agreementStatusId", agreement.getAgreementStatusId())
				.addValue("userId", userId)
				.addValue("startDate", agreement.getStartDate())
				.addValue("endDate", agreement.getEndDate())
				.addValue("cancellationDate", agreement.getCancellationDate())
				.addValue("procesingDate", agreement.getProcesingDate());
		this.namedJdbc.update(sql, params);
	}

	@Override
	public Integer findClientServiceByType(String serviceType) {
		String sql = "SELECT client_service_id\n"
				+ "FROM client_services.client_service WHERE \"type\" = :serviceType";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("serviceType", serviceType);
		try {
			return this.namedJdbc.queryForObject(sql, params, Integer.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Integer findAgreementByType(String agreementType) {
		String sql = "SELECT agreement_type_id\n"
				+ "FROM client_services.agreement_type WHERE \"type\" = :agreementType";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("agreementType", agreementType);
		try {
			return this.namedJdbc.queryForObject(sql, params, Integer.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Integer findBillingPlanByType(String billingPlanType) {
		String sql = "SELECT billing_plan_id\n"
				+ "FROM client_services.billing_plan WHERE \"type\" = :billingPlanType";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("billingPlanType", billingPlanType);
		try {
			return this.namedJdbc.queryForObject(sql, params, Integer.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Integer findAgreementStatusByStatus(String agreementStatus) {
		String sql = "SELECT agreement_status_id\n"
				+ "FROM client_services.agreement_status WHERE status = :agreementStatus";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("agreementStatus", agreementStatus);
		try {
			return this.namedJdbc.queryForObject(sql, params, Integer.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Boolean checkTermsConditions(Integer userId, Integer clientId) {
		String sql = "SELECT count(*) FROM client_services.terms_conditions WHERE user_id = :userId AND client_id = :clientId";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("clientId", clientId);
		try {
			return this.namedJdbc.queryForObject(sql, params, Integer.class) > 0 ? true : false;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Boolean existAgreement(Integer userId, Integer clientId, Integer serviceId) {
		String sql = "SELECT count(agreement_id)"
				+ "FROM client_services.agreement WHERE client_id = :clientId  AND user_id = :userId  AND client_service_id = :serviceId AND cancellation_date IS NULL";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("clientId", clientId)
				.addValue("userId", userId)
				.addValue("serviceId", serviceId);
		try {
			return this.namedJdbc.queryForObject(sql, params, Integer.class) > 0 ? true : false;
		} catch (Exception e) {
			return null;
		}	}

	@Override
	public void updateDemoAgreement(Integer agreementId) {
		String sql = "UPDATE client_services.agreement SET cancellation_date = now() WHERE agreement_id = :agreementId";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("agreementId", agreementId);
		this.namedJdbc.update(sql, params);		
	}
	
	@Override
	public Agreement findAgreementByService(Integer clientId, Integer userId, Integer serviceId) {
		String sql = "SELECT agreement_id, client_service_id, agreement_type_id, billing_plan_id, \n"
				+ "client_id, agreement_status_id, user_id, start_date, end_date, cancellation_date, procesing_date\n"
				+ "FROM client_services.agreement WHERE client_id = :clientId  AND user_id = :userId  AND client_service_id = :serviceId ";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("clientId", clientId)
				.addValue("userId", userId)
				.addValue("serviceId", serviceId);
		try {
			return (Agreement) this.namedJdbc.queryForObject(sql, params, new BeanPropertyRowMapper(Agreement.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public Integer findAgreementId(Integer clientId, Integer userId, Integer serviceId) {
		String sql = "SELECT agreement_id \n"
				+ "FROM client_services.agreement WHERE client_id = :clientId  AND user_id = :userId  AND client_service_id = :serviceId ";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("clientId", clientId)
				.addValue("userId", userId)
				.addValue("serviceId", serviceId);
		try {
			return this.namedJdbc.queryForObject(sql, params, Integer.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public String findAgreementType(Integer agreementTypeId) {
		String sql = "SELECT \"type\" \n"
				+ "FROM client_services.agreement_type WHERE agreement_type_id = :agreementTypeId";
		MapSqlParameterSource params = new MapSqlParameterSource()
				.addValue("agreementTypeId", agreementTypeId);
		return this.namedJdbc.queryForObject(sql, params, String.class);
	}

}
