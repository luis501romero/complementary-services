package com.motumweb.platform.servicesubscriptions.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.google.gson.Gson;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DataSourceConfig {
	
private final Logger LOG = LoggerFactory.getLogger(DataSourceConfig.class);
	
	@Value("${SERVICE_SUBSCRIPTIONS_ENV}")
	private String serviceSubscriptionsEnv;
	private Gson gson = new Gson();

	@Primary
	@Bean
	public HikariDataSource getDataSource() {
		HikariConfig config = null;
		try {
			config = gson.fromJson(this.serviceSubscriptionsEnv, HikariConfig.class);
		} catch (Exception e) {
			LOG.warn("Error reading environment variable", e);
		}
		HikariDataSource dataSource = new HikariDataSource(config);
		return dataSource;
	}

}
